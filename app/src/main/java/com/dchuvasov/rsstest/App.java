package com.dchuvasov.rsstest;

import android.app.Application;

import com.dchuvasov.rsstest.domain.RssSyncService;
import com.dchuvasov.rsstest.injection.AppComponent;
import com.dchuvasov.rsstest.injection.AppModule;
import com.dchuvasov.rsstest.injection.DaggerAppComponent;

import jonathanfinerty.once.Once;


/**
 * Created by Denis on 18.02.2017.
 */

public class App extends Application {
    private static final String INIT_SYNC_ON_INSTALL = "rss_sync";
    private static App instance;
    private AppComponent appComponent;

    public static App getInstance() {
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Once.initialise(this);
        instance = this;
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        scheduleSync();
    }

    private void scheduleSync() {
        if (!Once.beenDone(Once.THIS_APP_INSTALL, INIT_SYNC_ON_INSTALL)) {
            RssSyncService.scheduleSync(this);
            Once.markDone(INIT_SYNC_ON_INSTALL);
        }
    }


    public AppComponent appComponent() {
        return appComponent;
    }
}
