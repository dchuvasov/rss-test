package com.dchuvasov.rsstest.utils;

import android.util.Log;

import com.dchuvasov.rsstest.BuildConfig;

/**
 * Created by Denis on 18.02.2017.
 */
public class Logger {
    public static void d(String tag, String message) {
        if(BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }
}
