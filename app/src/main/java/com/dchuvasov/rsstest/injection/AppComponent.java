package com.dchuvasov.rsstest.injection;

import com.dchuvasov.rsstest.data.local.DbModule;
import com.dchuvasov.rsstest.domain.RssSyncService;
import com.dchuvasov.rsstest.presentation.MainActivity;
import com.dchuvasov.rsstest.presentation.news.NewsListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Denis on 18.02.2017.
 */

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        DomainModule.class,
        ApiModule.class,
        DbModule.class
})
public interface AppComponent {
    void inject(MainActivity mainActivity);

    void inject(NewsListFragment newsListFragment);

    void inject(RssSyncService rssSyncService);
}
