package com.dchuvasov.rsstest.injection;

import com.dchuvasov.rsstest.data.local.NewsLocalDataSource;
import com.dchuvasov.rsstest.data.remote.NewsRemoteDataSource;
import com.dchuvasov.rsstest.domain.NewsDataSource;
import com.dchuvasov.rsstest.domain.NewsRepository;
import com.dchuvasov.rsstest.network.RssService;
import com.dchuvasov.rsstest.network.entiry.RssRequest;
import com.squareup.sqlbrite.BriteDatabase;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Denis on 18.02.2017.
 */
@Module
public class DomainModule {
    public static final String JOB = "JOB";
    public static final String UI = "UI";

    @Provides
    @Singleton
    @Named(JOB)
    public Scheduler provideJobScheduler() {
        return Schedulers.newThread();
    }

    @Provides
    @Singleton
    @Named(UI)
    public Scheduler provideUIScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    public NewsRemoteDataSource provideRemoteDataSource(RssService service, RssRequest request) {
        return new NewsRemoteDataSource(service, request);
    }

    @Provides
    @Singleton
    public NewsDataSource provideLocalDataSource(BriteDatabase db) {
        return new NewsLocalDataSource(db);
    }

    @Provides
    @Singleton
    public NewsRepository provideRepository(NewsDataSource localDataSource, NewsRemoteDataSource remoteDataSource) {
        return new NewsRepository(localDataSource, remoteDataSource);
    }
}
