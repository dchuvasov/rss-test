package com.dchuvasov.rsstest.injection;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Denis on 18.02.2017.
 */

@Module
public class AppModule {
    private Context context;

    public AppModule(Application application) {
        this.context = application;
    }

    @Provides
    @Singleton
    public Context providesContext() {
        return context;
    }
}
