package com.dchuvasov.rsstest.injection;

import com.dchuvasov.rsstest.BuildConfig;
import com.dchuvasov.rsstest.network.LoggingInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Denis on 18.02.2017.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    CallAdapter.Factory providesCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Provides
    @Singleton
    LoggingInterceptor providesLoggingInterceptor() {
        return new LoggingInterceptor();
    }

    @Provides
    @Singleton
    Converter.Factory provideConvertFactory() {
        return SimpleXmlConverterFactory.create();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(CallAdapter.Factory callAdapterFactory,
                              LoggingInterceptor loggingInterceptor,
                              Converter.Factory convertFactory) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(loggingInterceptor);

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.ENDPOINT)
                .addCallAdapterFactory(callAdapterFactory)
                .addConverterFactory(convertFactory)
                .client(httpClient.build())
                .build();
    }
}
