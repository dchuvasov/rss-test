package com.dchuvasov.rsstest.injection;

import com.dchuvasov.rsstest.network.RssService;
import com.dchuvasov.rsstest.network.entiry.RssRequest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Denis on 18.02.2017.
 */

@Module
public class ApiModule {

    @Provides
    @Singleton
    RssService providesRssApi(Retrofit retrofit) {
        return retrofit.create(RssService.class);
    }

    @Provides
    @Singleton
    RssRequest provideRssRequest() {
        return new RssRequest(1, 3, 21);
    }
}
