package com.dchuvasov.rsstest.data.remote;

import com.dchuvasov.rsstest.domain.News;
import com.dchuvasov.rsstest.network.RssService;
import com.dchuvasov.rsstest.network.entiry.RssRequest;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Denis on 18.02.2017.
 */
public class NewsRemoteDataSource {
    private RssService service;
    private RssRequest request;

    @Inject
    public NewsRemoteDataSource(RssService service, RssRequest request) {
        this.service = service;
        this.request = request;
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    public Observable<List<News>> getNews() {
        return service.rss(request.getSubtype(), request.getCategory(), request.getCity())
                .map(rssChannel -> rssChannel.getChannel().getRssNewsList())
                .flatMap(Observable::from)
                .map(rssNews -> new News(rssNews.getTitle(), rssNews.getDescription(), rssNews.getLink(), rssNews.getPublishDate(), rssNews.getGuid(), null))
                .toList();
    }
}
