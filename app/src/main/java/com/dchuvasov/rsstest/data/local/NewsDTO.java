package com.dchuvasov.rsstest.data.local;

import android.content.ContentValues;
import android.provider.BaseColumns;

import com.dchuvasov.rsstest.domain.News;

/**
 * Created by Denis on 18.02.2017.
 */
public class NewsDTO implements BaseColumns {
    public static final String TABLE = "news_item";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String DESCRIPTION = "description";
    public static final String PUBLISH_DATE = "pubDate";
    public static final String GUID = "guid";

    public static final class Builder {
        private final ContentValues values = new ContentValues();

        public Builder id(long id) {
            values.put(_ID, id);
            return this;
        }

        public Builder title(String title) {
            values.put(TITLE, title);
            return this;
        }

        public Builder description(String description) {
            values.put(DESCRIPTION, description);
            return this;
        }

        public Builder link(String link) {
            values.put(LINK, link);
            return this;
        }

        public Builder publishDate(String publishDate) {
            values.put(PUBLISH_DATE, publishDate);
            return this;
        }

        public Builder guid(String guid) {
            values.put(GUID, guid);
            return this;
        }



        public Builder news(News news) {
            return title(news.getTitle())
                    .description(news.getDescription())
                    .link(news.getLink())
                    .publishDate(news.getPublishDate())
                    .guid(news.getGuid());
        }

        public ContentValues build() {
            return values;
        }
    }
}
