package com.dchuvasov.rsstest.data.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.dchuvasov.rsstest.domain.News;
import com.dchuvasov.rsstest.domain.NewsDataSource;
import com.squareup.sqlbrite.BriteDatabase;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Denis on 18.02.2017.
 */

public class NewsLocalDataSource implements NewsDataSource {

    private BriteDatabase database;

    @NonNull
    private Func1<Cursor, News> newsMapperFunction;

    public NewsLocalDataSource(BriteDatabase database) {
        this.database = database;
        newsMapperFunction = this::mapNews;
    }

    @NonNull
    private News mapNews(@NonNull Cursor c) {
        String title = Db.getString(c, NewsDTO.TITLE);
        String description = Db.getString(c, NewsDTO.DESCRIPTION);
        String link = Db.getString(c, NewsDTO.LINK);
        String publishDate = Db.getString(c, NewsDTO.PUBLISH_DATE);
        String guid = Db.getString(c, NewsDTO.GUID);

        return new News(title, description, link, publishDate, guid, null);
    }

    @Override
    public Observable<List<News>> getNews() {
        String[] projection = {
                NewsDTO.TITLE,
                NewsDTO.DESCRIPTION,
                NewsDTO.LINK,
                NewsDTO.PUBLISH_DATE,
                NewsDTO.GUID
        };

        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection), NewsDTO.TABLE);
        return database.createQuery(NewsDTO.TABLE, sql)
                .mapToList(newsMapperFunction);
    }

    @Override
    public void saveNews(News news) {
        ContentValues values = new NewsDTO.Builder()
                .news(news)
                .build();

        database.insert(NewsDTO.TABLE, values, SQLiteDatabase.CONFLICT_REPLACE);
    }
}
