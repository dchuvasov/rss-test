package com.dchuvasov.rsstest.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.BuildConfig;
import com.squareup.sqlbrite.SqlBrite;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.schedulers.Schedulers;

/**
 * Created by Denis on 18.02.2017.
 */
@Module
public class DbModule {

    @Provides
    @Singleton
    SQLiteOpenHelper provideOpenHelper(Context context) {
        return new DbOpenHelper(context);
    }

    @Provides
    @Singleton
    SqlBrite provideSqlBrite() {
        return SqlBrite.create(new SqlBrite.Logger() {
            @Override
            public void log(String message) {
                Log.d("DB", message);
            }
        });
    }

    @Provides
    @Singleton
    BriteDatabase provideDatabase(SqlBrite sqlBrite, SQLiteOpenHelper hepler) {
        BriteDatabase db = sqlBrite.wrapDatabaseHelper(hepler, Schedulers.io());
        db.setLoggingEnabled(BuildConfig.DEBUG);
        return db;
    }
}
