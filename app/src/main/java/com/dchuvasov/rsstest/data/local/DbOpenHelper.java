package com.dchuvasov.rsstest.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Denis on 18.02.2017.
 */
public class DbOpenHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;

    private static final String CREATE_NEWS = ""
            + "CREATE TABLE " + NewsDTO.TABLE + "("
            + NewsDTO._ID + " INTEGER NOT NULL PRIMARY KEY,"
            + NewsDTO.TITLE + " TEXT NOT NULL,"
            + NewsDTO.DESCRIPTION + " TEXT NOT NULL,"
            + NewsDTO.LINK + " TEXT NOT NULL,"
            + NewsDTO.PUBLISH_DATE + " TEXT NOT NULL,"
            + NewsDTO.GUID + " TEXT NOT NULL"

            + ")";

    public DbOpenHelper(Context context) {
        super(context, "rss.db", null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_NEWS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
