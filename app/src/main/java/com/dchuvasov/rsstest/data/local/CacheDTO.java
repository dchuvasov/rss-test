package com.dchuvasov.rsstest.data.local;

import android.provider.BaseColumns;

/**
 * Created by Denis on 19.02.2017.
 */

public class CacheDTO implements BaseColumns {
    public static final String TABLE = "cache_table";
    public static final String GUID = "guid";
    public static final String CACHE_URI = "cache_uri";
}
