package com.dchuvasov.rsstest.network;

import com.dchuvasov.rsstest.network.entiry.RssChannel;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Denis on 18.02.2017.
 */

public interface RssService {

    @GET("_/rss/_rss.html")
    Observable<RssChannel> rss(@Query("subtype") int subtype,
                               @Query("category") int category,
                               @Query("city") int city);
}
