package com.dchuvasov.rsstest.network.entiry;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Denis on 18.02.2017.
 */
@Root(name = "rss", strict = false)
public class RssChannel {
    @Element
    private RssFeed channel;

    public RssFeed getChannel() {
        return channel;
    }
}
