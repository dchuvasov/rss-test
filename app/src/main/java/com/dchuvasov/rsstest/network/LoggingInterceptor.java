package com.dchuvasov.rsstest.network;

import com.dchuvasov.rsstest.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Denis on 18.02.2017.
 */
public class LoggingInterceptor implements Interceptor {
    private final Interceptor interceptor;

    public LoggingInterceptor() {
        interceptor = new HttpLoggingInterceptor().
                setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        return interceptor.intercept(chain);
    }
}
