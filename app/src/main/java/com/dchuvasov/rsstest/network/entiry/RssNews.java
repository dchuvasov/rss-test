package com.dchuvasov.rsstest.network.entiry;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Denis on 18.02.2017.
 */
@Root(name="item",strict = false)
public class RssNews {
    @Element(name = "title")
    private String title;

    @Element(name = "link")
    private String link;

    @Element(name = "description")
    private String description;

    @Element(name = "pubDate")
    private String publishDate;

    @Element(name = "guid")
    private String guid;

    public RssNews() {
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public String getGuid() {
        return guid;
    }
}
