package com.dchuvasov.rsstest.network.entiry;

/**
 * Created by Denis on 18.02.2017.
 */

public class RssRequest {
    private int subtype;
    private int category;
    private int city;

    public RssRequest(int subtype, int category, int city) {
        this.subtype = subtype;
        this.category = category;
        this.city = city;
    }

    public int getSubtype() {
        return subtype;
    }

    public int getCategory() {
        return category;
    }

    public int getCity() {
        return city;
    }
}
