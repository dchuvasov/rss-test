package com.dchuvasov.rsstest.network.entiry;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis on 18.02.2017.
 */
@Root(name = "channel",strict = false)
public class RssFeed {
    @ElementList(inline = true)
    protected List<RssNews> rssNewsList;

    public List<RssNews> getRssNewsList() {
        if (rssNewsList == null)
            rssNewsList = new ArrayList<>();
        return rssNewsList;
    }
}
