package com.dchuvasov.rsstest.domain;

import android.content.Context;
import android.util.Log;

import com.dchuvasov.rsstest.App;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.TaskParams;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

public class RssSyncService extends GcmTaskService {
    private static final String TAG = "RssSyncService";
    private static final long SYNC_PERIOD_SECONDS = 5 * 60L;
    private static final String PERIODIC_SYNC_TAG = "periodic_sync_tag";

    @Inject
    NewsRepository repository;

    public static void scheduleSync(Context ctx) {
        GcmNetworkManager gcmNetworkManager = GcmNetworkManager.getInstance(ctx);
        PeriodicTask periodicTask = new PeriodicTask.Builder()
                .setPeriod(SYNC_PERIOD_SECONDS)
                .setRequiredNetwork(PeriodicTask.NETWORK_STATE_CONNECTED)
                .setTag(PERIODIC_SYNC_TAG)
                .setService(RssSyncService.class)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build();

        gcmNetworkManager.schedule(periodicTask);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() called");
        App.getInstance().appComponent().inject(this);
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.d(TAG, "onRunTask() called with: taskParams = [" + taskParams + "]");
        repository.syncNews()
                .subscribe(new Subscriber<List<News>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted() called");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError() called with: e = [" + e + "]");
                    }

                    @Override
                    public void onNext(List<News> newses) {
                        Log.d(TAG, "onNext() called with: newses = [" + newses + "]");
                    }
                });
        return GcmNetworkManager.RESULT_SUCCESS;
    }
}
