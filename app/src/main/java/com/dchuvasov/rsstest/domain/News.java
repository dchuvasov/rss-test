package com.dchuvasov.rsstest.domain;

/**
 * Created by Denis on 18.02.2017.
 */

public class News {
    private String title;
    private String description;
    private String link;
    private String publishDate;
    private String guid;
    private String cacheUri;

    public News(String title, String description, String link, String publishDate, String guid, String cacheUri) {
        this.title = title;
        this.description = description;
        this.link = link;
        this.publishDate = publishDate;
        this.guid = guid;
        this.cacheUri = cacheUri;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public String getCacheUri() {
        return cacheUri;
    }

    public String getGuid() {
        return guid;
    }

    public boolean isFavorite() {
        return cacheUri != null;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                '}';
    }
}