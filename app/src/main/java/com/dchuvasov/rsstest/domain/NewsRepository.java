package com.dchuvasov.rsstest.domain;

import android.util.Log;

import com.dchuvasov.rsstest.data.remote.NewsRemoteDataSource;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Denis on 18.02.2017.
 */

public class NewsRepository implements NewsDataSource {
    private static final String TAG = "NewsRepository";
    private final NewsDataSource localDataSource;
    private final NewsRemoteDataSource remoteDataSource;

    private boolean cacheIsDirty;
    private Map<String, News> cacheNews;

    public NewsRepository(NewsDataSource localDataSource,
                          NewsRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Observable<List<News>> getNews() {
        if(cacheNews != null && !cacheIsDirty) {
            return Observable.from(cacheNews.values()).toList();
        }
        else if (cacheNews == null) {
            cacheNews = new LinkedHashMap<>();
        }

        Observable<List<News>> remoteNews = getAndSaveNews();

        if(cacheIsDirty) {
            return remoteNews;
        }

        Observable<List<News>> localNews = getAndCacheLocalNews();

        return Observable.concat(localNews, remoteNews)
                .filter(newsList -> !newsList.isEmpty())
                .first();
}

    @Override
    public void saveNews(News news) {

    }

    public void refreshNews() {
        cacheIsDirty = true;
    }

    @RxLogObservable(RxLogObservable.Scope.EVERYTHING)
    private Observable<List<News>> getAndSaveNews() {
        return remoteDataSource
                .getNews()
                .flatMap(new Func1<List<News>, Observable<List<News>>>() {
                    @Override
                    public Observable<List<News>> call(List<News> newsList) {
                        return Observable.from(newsList)
                                .doOnNext(news -> {
                                    Log.d(TAG, "call() called with: news = [" + news + "]");
                                    localDataSource.saveNews(news);
                                    cacheNews.put(news.getGuid(), news);
                                }).toList();
                    }
                }).doOnCompleted(() -> cacheIsDirty = false);
    }

    private Observable<List<News>> getAndCacheLocalNews() {
        return localDataSource.getNews()
                .flatMap(new Func1<List<News>, Observable<List<News>>>() {
                    @Override
                    public Observable<List<News>> call(List<News> newsList) {
                        return Observable.from(newsList)
                                .doOnNext(news -> cacheNews.put(news.getGuid(), news))
                                .toList();
                    }
                });
    }

    public Observable<List<News>> syncNews() {
        return getAndSaveNews();
    }
}
