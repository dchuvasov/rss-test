package com.dchuvasov.rsstest.domain;

import java.util.List;

import rx.Observable;

/**
 * Created by Denis on 18.02.2017.
 */

public interface NewsDataSource {
    Observable<List<News>> getNews();

    void saveNews(News news);
}
