package com.dchuvasov.rsstest.presentation.common;

import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * Created by Denis on 19.02.2017.
 */

public class BasePresenter<V extends MvpView> {
    private WeakReference<V> viewRef;

    public void attachView(V view) {
        viewRef = new WeakReference<V>(view);
    }


    @Nullable
    public V getView() {
        return viewRef == null ? null : viewRef.get();
    }

    public boolean isViewAttached() {
        return viewRef != null && viewRef.get() != null;
    }


     public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }
}
