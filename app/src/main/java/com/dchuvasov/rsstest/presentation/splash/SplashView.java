package com.dchuvasov.rsstest.presentation.splash;

import com.dchuvasov.rsstest.presentation.common.MvpView;

/**
 * Created by Denis on 19.02.2017.
 */

public interface SplashView extends MvpView {
    void showMainScreen();
}
