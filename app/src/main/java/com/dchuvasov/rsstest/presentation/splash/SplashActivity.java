package com.dchuvasov.rsstest.presentation.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import com.dchuvasov.rsstest.R;
import com.dchuvasov.rsstest.common.BaseActivity;
import com.dchuvasov.rsstest.common.Layout;
import com.dchuvasov.rsstest.domain.RssSyncService;
import com.dchuvasov.rsstest.presentation.MainActivity;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

import butterknife.OnClick;

@Layout(R.layout.activity_splash)
public class SplashActivity extends BaseActivity implements SplashView {
    SplashPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SplashPresenter();
        presenter.attachView(this);
    }

    @OnClick(R.id.btn_splash_next)
    void onNextClick(View view) {
        presenter.onNextClick();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
