package com.dchuvasov.rsstest.presentation.news;

import android.util.Log;

import com.dchuvasov.rsstest.domain.News;
import com.dchuvasov.rsstest.domain.NewsRepository;
import com.dchuvasov.rsstest.presentation.common.BasePresenter;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Denis on 19.02.2017.
 */

public class NewsListPresenter extends BasePresenter<NewsListView> {
    private static final String TAG = "NewsListPresenter";

    NewsRepository repository;

    public NewsListPresenter(NewsRepository repository) {
        this.repository = repository;
    }

    @Override
    public void attachView(NewsListView view) {
        super.attachView(view);
        loadNews();
    }

    private void loadNews() {
        repository.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<News> newses) {
                        if(isViewAttached())
                            getView().setNews(newses);
                    }
                });
    }

    public void refresh() {
        repository.refreshNews();
        loadNews();
    }
}
