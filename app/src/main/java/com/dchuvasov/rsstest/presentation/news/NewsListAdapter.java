package com.dchuvasov.rsstest.presentation.news;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dchuvasov.rsstest.R;
import com.dchuvasov.rsstest.domain.News;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Denis on 19.02.2017.
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsItemViewHolder> {
    private List<News> data = new ArrayList<>();

    @Override
    public NewsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_news, parent, false);
        return new NewsItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsItemViewHolder holder, int position) {
        holder.bindView(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setNews(List<News> newsList) {
        data.clear();
        data.addAll(newsList);
        notifyDataSetChanged();
    }

    static class NewsItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_list_news_title)
        TextView tvTitle;

        public NewsItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(News news) {
            tvTitle.setText(news.getTitle());
        }
    }
}
