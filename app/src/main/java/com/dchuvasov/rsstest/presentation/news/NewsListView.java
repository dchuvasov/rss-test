package com.dchuvasov.rsstest.presentation.news;

import com.dchuvasov.rsstest.domain.News;
import com.dchuvasov.rsstest.presentation.common.MvpView;

import java.util.List;

/**
 * Created by Denis on 19.02.2017.
 */

public interface NewsListView extends MvpView {
    void setNews(List<News> newsList);
}
