package com.dchuvasov.rsstest.presentation.news;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dchuvasov.rsstest.App;
import com.dchuvasov.rsstest.R;
import com.dchuvasov.rsstest.common.BaseFragment;
import com.dchuvasov.rsstest.common.Layout;
import com.dchuvasov.rsstest.domain.News;
import com.dchuvasov.rsstest.domain.NewsRepository;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;


@Layout(R.layout.fragment_news)
public class NewsListFragment extends BaseFragment
        implements SwipeRefreshLayout.OnRefreshListener, NewsListView {

    @BindView(R.id.srl_news_refresh)
    SwipeRefreshLayout srlRefresh;

    @BindView(R.id.rv_news_list)
    RecyclerView rvList;

    NewsListAdapter adapter;

    @Inject
    NewsRepository repository;

    NewsListPresenter presenter;

    public static NewsListFragment newInstance() {
        return new NewsListFragment();
    }

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getInstance().appComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new NewsListAdapter();
        rvList.setAdapter(adapter);
        srlRefresh.setOnRefreshListener(this);
        presenter = new NewsListPresenter(repository);
        presenter.attachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    public void setNews(List<News> newsList) {
        adapter.setNews(newsList);
        srlRefresh.setRefreshing(false);
    }
}
