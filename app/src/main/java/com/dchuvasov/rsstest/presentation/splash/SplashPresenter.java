package com.dchuvasov.rsstest.presentation.splash;

import com.dchuvasov.rsstest.presentation.common.BasePresenter;

/**
 * Created by Denis on 19.02.2017.
 */

public class SplashPresenter extends BasePresenter<SplashView> {

    public void onNextClick() {
        if(isViewAttached())
            getView().showMainScreen();
    }
}
