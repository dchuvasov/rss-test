package com.dchuvasov.rsstest.common;

/**
 * Created by Denis on 18.02.2017.
 */

public interface BackButtonListener {
    boolean onBackPressed();
}
