package com.dchuvasov.rsstest.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import java.lang.annotation.Annotation;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Denis on 18.02.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Class cls = getClass();
        if (!cls.isAnnotationPresent(Layout.class))
            throw new IllegalArgumentException();

        Annotation annotation = cls.getAnnotation(Layout.class);
        Layout layout = (Layout) annotation;

        setContentView(layout.value());

        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(rootForFragmentId());

        if (fragment == null ||
                !(fragment instanceof BackButtonListener)
                || !((BackButtonListener) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    protected int rootForFragmentId() {
        return -1;
    }
}
